//
//  CustomDCPClass.swift
//  DCPLaraCreamer
//
//  Created by Lara Creamer on 5/16/16.
//  Copyright © 2016 Lara Creamer. All rights reserved.
//

import Foundation
import Firebase

class CustomDCPClass {
    
    var busName: String?
    var arrivalTime: String?
    var status: String?
    var route: String?

    
    
    init(busName: String, arrivalTime: String, status: String, route: String) {
        self.busName = busName
        self.arrivalTime = arrivalTime
        self.status = status
        self.route = route
    }
    
//    var busA1: String?
//
//    var busA2: String?
//
//    var busA3: String?
//
//    var busB1: String?
//
//    var busB2: String?
//
//    var busB3: String?
//
//    var busC1: String?
//
//    var busC2: String?
//
//    var busC3: String?
//
//    var busD1: String?
//
//    var busD2: String?
//    
//    var busD3: String?
//
//    init(busA1: String, busA2: String, busA3: String, busB1: String, busB2: String, busB3: String, busC1: String, busC2: String, busC3: String, busD1: String, busD2: String, busD3: String) {
//        
//        self.busA1 = busA1
//        self.busA2 = busA2
//        self.busA3 = busA3
//        self.busB1 = busB1
//        self.busB2 = busB2
//        self.busB3 = busB3
//        self.busC1 = busC1
//        self.busC2 = busC2
//        self.busC3 = busC3
//        self.busD1 = busD1
//        self.busD2 = busD2
//        self.busD3 = busD3
//    }
    
    
    
}
