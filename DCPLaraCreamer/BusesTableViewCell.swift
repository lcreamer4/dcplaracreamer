//
//  BusesTableViewCell.swift
//  DCPLaraCreamer
//
//  Created by Lara Creamer on 5/11/16.
//  Copyright © 2016 Lara Creamer. All rights reserved.
//

import UIKit

class BusesTableViewCell: UITableViewCell {
    
    @IBOutlet weak var busName: UILabel!
    
    @IBOutlet weak var arrivalTime: UILabel!

    @IBOutlet weak var statusImage: UIImageView!
    
    @IBOutlet weak var busPark: UILabel!
    
    @IBOutlet weak var timeLate: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
