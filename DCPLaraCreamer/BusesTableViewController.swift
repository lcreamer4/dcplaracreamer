//
//  BusesTableViewController.swift
//  DCPLaraCreamer
//
//  Created by Lara Creamer on 5/11/16.
//  Copyright © 2016 Lara Creamer. All rights reserved.
//

import UIKit
import Firebase

class BusesTableViewController: UITableViewController {
    
    var statusImage = UIImage?()
    var items = [FDataSnapshot]()
    var newItems = [FDataSnapshot]()
    var busNameArray: [String] = []
    var arrivalTimeArray: [String] = []
    var statusArray: [String] = []
    var statusImageArray: [String] = []
    var busParkArray: [String] = []
    var timeLateArray: [String] = []
    var busRouteArray: [String] = []
    var busName = ""
    var status = ""
    var arrivalTimes = ""
    var timeLate = ""
    var busRoute = ""
    var rowNumber = 0
    

    @IBAction func logOut(sender: UIBarButtonItem) {
        let alert = UIAlertController(title: "Logging Out?", message: "Are you sure you want to log out?", preferredStyle: .Alert)
        
        alert.addAction(UIAlertAction(title: "Log Out", style: .Default, handler: { (alertAction) -> Void in
            
            self.performSegueWithIdentifier("toLogOutScreen", sender: self)
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .Cancel, handler: nil))
        
        presentViewController(alert, animated: true, completion: nil)
    }

    
    func refresh(sender:AnyObject)
    {
        // Updating your data here...
        
        self.tableView.reloadData()
        self.refreshControl?.endRefreshing()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        

        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    
    }
    
    override func viewWillAppear(animated: Bool) {
        //Maybe clearing the entire table and repopulating.
        self.busNameArray.removeAll()
        
        self.statusArray.removeAll()
        
        self.arrivalTimeArray.removeAll()
        
        self.statusImageArray.removeAll()
        
        self.arrivalTimeArray.removeAll()
        
        self.timeLateArray.removeAll()
        
        self.busRouteArray.removeAll()
        
        
        let myRef = Firebase(url:"https://burning-inferno-6410.firebaseio.com/buses")
        
        myRef.queryOrderedByKey().observeEventType(.ChildAdded, withBlock: { snapshot in
            
            if let busName = snapshot.value["bus name"] as? String {
                self.busNameArray.append(busName)
            }
            if let arrivalTime = snapshot.value["arrival time"] as? String {
                self.arrivalTimeArray.append(arrivalTime)
            }
            if let busStatus = snapshot.value["status"] as? String {
                self.statusArray.append(busStatus)
            }
            if let busImage = snapshot.value["park"] as? String {
                self.busParkArray.append(busImage)
            }
            if let statusImage = snapshot.value["status Image"] as? String {
                self.statusImageArray.append(statusImage)
            }
            if let timeLate = snapshot.value["time late"] as? String {
                self.timeLateArray.append(timeLate)
            }
            if let busRoute = snapshot.value["route"] as? String {
                self.busRouteArray.append(busRoute)
            }
            dispatch_async(dispatch_get_main_queue(), {
                self.tableView.reloadData()
            })
        })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return busNameArray.count
        
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as! BusesTableViewCell

    
        // Configure the cell...
        cell.statusImage.image = UIImage(named:statusImageArray[indexPath.row])
        cell.timeLate.text = timeLateArray[indexPath.row]
        if cell.timeLate.text == "0 minutes" || cell.timeLate.text == "On Time" {
            cell.timeLate.hidden = true
        } else {
            cell.timeLate.hidden = false
        }
        cell.busName.text = busNameArray[indexPath.row]
        cell.arrivalTime.text = statusArray[indexPath.row]
        cell.busPark.text = busParkArray[indexPath.row]
        return cell
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "toDetailView" {
        let destinationVC = segue.destinationViewController as! BusDetailViewController
        destinationVC.currentStatus = timeLate
            destinationVC.busName = busName
            destinationVC.arrivalTimes = arrivalTimes
            destinationVC.busRoute = busRoute
        }
    }
    
    override func tableView(tableView: UITableView, didHighlightRowAtIndexPath indexPath: NSIndexPath) {
        busName = busNameArray[indexPath.row]
        status = statusArray[indexPath.row]
        arrivalTimes = arrivalTimeArray[indexPath.row]
        busRoute = busRouteArray[indexPath.row]
        timeLate = timeLateArray[indexPath.row]
        rowNumber = indexPath.row
        print(rowNumber)
    }
    
    @IBAction func unwindSegue(segue: UIStoryboardSegue){
//        let source = segue.sourceViewController as! BusDetailViewController
//        
//        var indexPath = NSIndexPath(forRow: rowNumber, inSection: 0)
//        self.tableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Top)
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
