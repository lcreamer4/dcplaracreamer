//
//  EventDetailViewController.swift
//  DCPLaraCreamer
//
//  Created by Lara Creamer on 5/16/16.
//  Copyright © 2016 Lara Creamer. All rights reserved.
//

import UIKit
import Firebase

class EventDetailViewController: UIViewController {
    
    var eventNameCatch = ""
    var eventDateTimeCatch = ""
    var eventLocationCatch = ""
    var eventDescriptionCatch = ""
    
    @IBOutlet weak var eventNameOutlet: UILabel!
    
    @IBOutlet weak var eventTimeDateOutlet: UILabel!
    
    @IBOutlet weak var eventLocationOutlet: UILabel!
    
    @IBOutlet weak var eventDescriptionOutlet: UITextView!
    

    override func viewDidLoad() {
        super.viewDidLoad()
     
        eventNameOutlet.text = eventNameCatch
        eventTimeDateOutlet.text = eventDateTimeCatch
        eventLocationOutlet.text = eventLocationCatch
        eventDescriptionOutlet.text = eventDescriptionCatch
        

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
