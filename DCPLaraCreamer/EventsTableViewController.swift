//
//  EventsTableViewController.swift
//  DCPLaraCreamer
//
//  Created by Lara Creamer on 5/16/16.
//  Copyright © 2016 Lara Creamer. All rights reserved.
//

import UIKit
import Firebase

class EventsTableViewController: UITableViewController {

    var eventDateArray: [String] = []
    var eventTimeArray: [String] = []
    var eventLocationArray: [String] = []
    var eventNameArray: [String] = []
    var eventDescArray: [String] = []
    var sortedDateArray: [String] = []
    var selectedEvent = ""
    var selectedDate = ""
    var selectedLocation = ""
    var selectedTime = ""
    var selectedDescription = ""
    
    
    @IBAction func logOutButton(sender: UIBarButtonItem) {
        let alert = UIAlertController(title: "Logging Out?", message: "Are you sure you want to log out?", preferredStyle: .Alert)
        
        alert.addAction(UIAlertAction(title: "Log Out", style: .Default, handler: { (alertAction) -> Void in
            
            self.performSegueWithIdentifier("toLogOutScreen", sender: self)
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .Cancel, handler: nil))
        
        presentViewController(alert, animated: true, completion: nil)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let myRef = Firebase(url:"https://burning-inferno-6410.firebaseio.com/events")
        
        myRef.queryOrderedByKey().observeEventType(.ChildAdded, withBlock: { snapshot in
            
            if let eventDate = snapshot.value["date"] as? String {
                self.eventDateArray.append(eventDate)
                
                self.sortedDateArray = self.eventDateArray.sort()
            }
            
            if let eventTime = snapshot.value["time"] as? String {
                self.eventTimeArray.append(eventTime)
            }
            
            if let eventLocation = snapshot.value["location"] as? String {
                self.eventLocationArray.append(eventLocation)
            }
            
            if let eventName = snapshot.value["event name"] as? String {
                self.eventNameArray.append(eventName)
            }
            if let eventDescription = snapshot.value["description"] as? String {
                self.eventDescArray.append(eventDescription)
            }
            dispatch_async(dispatch_get_main_queue(), {
                self.tableView.reloadData()
            })
        })
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return eventNameArray.count
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as! EventsTableViewCell

        // Configure the cell...
        
        cell.eventName.text = eventNameArray[indexPath.row]
        cell.eventDate.text = eventDateArray[indexPath.row]
        cell.eventLocation.text = eventLocationArray[indexPath.row]

        return cell
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "toDetailView" {
            let destinationVC = segue.destinationViewController as! EventDetailViewController
            destinationVC.eventNameCatch = selectedEvent
            destinationVC.eventDateTimeCatch = "\(selectedDate) from \(selectedTime)"
            destinationVC.eventLocationCatch = selectedLocation
            destinationVC.eventDescriptionCatch = selectedDescription
        }
    }
    
    override func tableView(tableView: UITableView, didHighlightRowAtIndexPath indexPath: NSIndexPath) {
        selectedEvent = eventNameArray[indexPath.row]
        selectedDate = eventDateArray[indexPath.row]
        selectedLocation = eventLocationArray[indexPath.row]
        selectedTime = eventTimeArray[indexPath.row]
        selectedDescription = eventDescArray[indexPath.row]
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
