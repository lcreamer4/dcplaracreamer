//
//  ShiftTableViewCell.swift
//  DCPLaraCreamer
//
//  Created by Lara Creamer on 5/17/16.
//  Copyright © 2016 Lara Creamer. All rights reserved.
//

import UIKit

class ShiftTableViewCell: UITableViewCell {
    
    @IBOutlet weak var occupationLabel: UILabel!
    
    @IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var locationLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
