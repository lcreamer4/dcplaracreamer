//
//  LogOutViewController.swift
//  DCPLaraCreamer
//
//  Created by Lara Creamer on 5/13/16.
//  Copyright © 2016 Lara Creamer. All rights reserved.
//

import UIKit

class LogOutViewController: UIViewController {
    
    
    @IBOutlet weak var buttonView: UIView!
    
    @IBAction func backtoLogin(sender: UIButton) {
        performSegueWithIdentifier("toLoginScreen", sender: nil)
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        buttonView.layer.cornerRadius = 8

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
