//
//  ViewController.swift
//  DCPLaraCreamer
//
//  Created by Lara Creamer on 5/2/16.
//  Copyright © 2016 Lara Creamer. All rights reserved.
//

import UIKit
import Firebase

class ViewController: UIViewController {
    
    @IBOutlet weak var usernameTextInput: UITextField!
    
    @IBOutlet weak var passwordTextInput: UITextField!
    
    @IBOutlet weak var usernameError: UILabel!
    
    @IBOutlet weak var passwordError: UILabel!
    
    @IBOutlet weak var buttonView: UIView!
    
    @IBAction func LogInButton(sender: UIButton) {
        if usernameTextInput.text == "Lcreamer" && passwordTextInput.text == "Jan03" {
            
            performSegueWithIdentifier("toHomeScreen", sender: self)
            
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       let myRef = Firebase(url:"https://burning-inferno-6410.firebaseio.com")
        
//        //Create the buses
        let A1 = ["bus name": "A1", "arrival time": "9:00, 10:00, 11:00, 12:00, 13:00, 14:00", "route": "VW -> TC -> CS -> Magic Kingdom", "status": "On Time", "time late": "0 minutes", "status Image": "clockOnTime", "park": "MK"]
        let A2 = ["bus name": "A2", "arrival time": "15:00, 16:00, 17:00, 18:00, 19:00", "route": "VW -> TC -> CS -> Magic Kingdom", "status": "On Time", "time late": "0 minutes", "status Image": "clockOnTime", "park": "MK"]
        let A3 = ["bus name": "A3", "arrival time": "20:00, 21:00, 22:00, 23:00, 24:00", "route": "VW -> TC -> CS -> Magic Kingdom" , "status": "On Time", "time late": "0 minutes","status Image": "clockOnTime", "park": "MK"]
        let B1 = ["bus name": "B1", "arrival time": "9:00, 10:00, 11:00, 12:00, 13:00, 14:00", "route": "VW -> TC -> CS -> Epcot", "status": "On Time", "time late": "0 minutes", "status Image": "clockOnTime", "park": "EP"]
        let B2 = ["bus name": "B2", "arrival time": "15:00, 16:00, 17:00, 18:00, 19:00", "route": "VW -> TC -> CS -> Epcot", "status": "On Time", "time late": "0 minutes", "status Image": "clockOnTime", "park": "EP"]
        let B3 = ["bus name": "B3", "arrival time": "20:00, 21:00, 22:00, 23:00, 24:00", "route": "VW -> TC -> CS -> Epcot", "status": "On Time", "time late": "0 minutes", "status Image": "clockOnTime", "park": "EP"]
        let C1 = ["bus name": "C1", "arrival time": "9:00, 10:00, 11:00, 12:00, 13:00, 14:00", "route": "VW -> TC -> CS -> Hollywood Studios", "status": "On Time", "time late": "0 minutes", "status Image": "clockOnTime", "park": "HS"]
        let C2 = ["bus name": "C2", "arrival time": "15:00, 16:00, 17:00, 18:00, 19:00", "route": "VW -> TC -> CS -> Hollywood Studios", "status": "On Time", "time late": "0 minutes", "status Image": "clockOnTime", "park": "HS"]
        let C3 = ["bus name": "C3", "arrival time": "20:00, 21:00, 22:00, 23:00, 24:00", "route": "VW -> TC -> CS -> Hollywood Studios", "status": "On Time", "time late": "0 minutes", "status Image": "clockOnTime", "park": "HS"]
        let D1 = ["bus name": "D1", "arrival time": "9:00, 10:00, 11:00, 12:00, 13:00, 14:00", "route": "VW -> TC -> CS -> Animal Kingdom", "status": "On Time", "time late": "0 minutes", "status Image": "clockOnTime", "park": "AK"]
        let D2 = ["bus name": "D2", "arrival time": "15:00, 16:00, 17:00, 18:00, 19:00", "route": "VW -> TC -> CS -> Animal Kingdom", "status": "On Time", "time late": "0 minutes", "status Image": "clockOnTime", "park": "AK"]
        let D3 = ["bus name": "D3", "arrival time": "20:00, 21:00, 22:00, 23:00, 24:00", "route": "VW -> TC -> CS -> Animal Kingdom", "status": "On Time", "time late": "0 minutes", "status Image": "clockOnTime", "park": "AK"]
        
        let busRef = myRef.childByAppendingPath("buses")
        
        let buses = ["A1": A1, "A2": A2, "A3": A3, "B1": B1, "B2": B2, "B3": B3, "C1": C1, "C2": C2, "C3": C3, "D1": D1, "D2": D2, "D3": D3  ]
        
        busRef.setValue(buses)

        //Create the events
        let typhoonParty = ["event name":"Waterpark Night", "date":"06/05/2016", "time":"7:00 - 12:00 PM", "location":"Typhoon Lagoon", "description":"Come make a splash at Typhoon Lagoon after hours! We have closed down the park as a special party for our Disney College Program students, and everything is open. The slides, the wavepool, and the skark reef are all available for students to use during this special after hours event, and food and drinks will be provided."]
        
        let movieNight = ["event name":"Movie Night", "date":"07/03/2016", "time":"7:00 - 12:00 PM", "location":"The Commons", "description":"Come out to the commons for a night of pizza and movies. With the new star wars recently released, we will have a short recap of the previous episodes, followed by viewing Star Wars episode 7. Pizza and drinks will be provided, and you can come and go as you please, so do not feel obigated to stay the whole time."]
        
        let paintParty = ["event name":"Paint Party", "date":"08/01/2016", "time":"7:00 - 12:00 PM", "location":"Vista Way", "description":"Come over to Vista Way for a nighttime paint party. Wear clothes that you dont mind getting messy, and feel free to request songs to the DJ. Pizza and drinks will be provided, so do not feel obligated to stay the whole time."]
        
        let icecreamParty = ["event name":"Ice cream Party", "date":"09/02/2016", "time":"4:00 - 8:00 PM", "location":"The Commons", "description":"In preparation for the end of the program, we are having an ice cream party at the Commons. An ice cream bar will be provided with different assorted flavors of icecream as well as toppings, and those that participate will be put in a drawing for DCP gear. "]
    
        let eventRef = myRef.childByAppendingPath("events")
        
        let events = ["typhoonParty": typhoonParty, "movieNight": movieNight, "paintParty": paintParty, "icecreamParty": icecreamParty]
        
        eventRef.setValue(events)
        
        let shift1 = ["shift name": "shift1", "shift occcupation": "Housekeeping", "shift date":"06/01", "shift time": "13:00-21:30", "shift location": "Contemporary"]
        let shift2 = ["shift name": "shift2","shift occcupation": "Lifeguard", "shift date":"06/05", "shift time": "13:00-21:30", "shift location": "Typhoon Lagoon"]
        let shift3 = ["shift name": "shift3","shift occcupation": "Food/Beverage", "shift date":"07/01", "shift time": "15:00-1:30", "shift location": "Magic Kingdom"]
        
       let shiftRef = myRef.childByAppendingPath("shifts")
       
       let shifts = ["shift1": shift1, "shift2": shift2, "shift3": shift3]
        
        shiftRef.setValue(shifts)

        let day1 = ["shift name": "day1","shift occcupation": "Lifeguard", "shift date":"06/01", "shift time": "13:00-21:30", "shift location": "Contemporary"]
        let day2 = ["shift name": "day2","shift occcupation": "Lifeguard", "shift date":"06/02", "shift time": "13:00-21:30", "shift location": "Contemporary"]
        let day3 = ["shift name": "day3","shift occcupation": "Lifeguard", "shift date":"06/03", "shift time": "13:00-21:30", "shift location": "Contemporary"]
        let day4 = ["shift name": "day4","shift occcupation": "Lifeguard", "shift date":"06/04", "shift time": "13:00-21:30", "shift location": "Contemporary"]
        let day5 = ["shift name": "day5","shift occcupation": "Lifeguard", "shift date":"06/05", "shift time": "13:00-21:30", "shift location": "Contemporary"]
        
        let mySchedRef = myRef.childByAppendingPath("mySchedule")
        
        let myShifts = ["day1": day1, "day2": day2, "day3": day3, "day4": day4, "day5": day5]
        
        mySchedRef.setValue(myShifts)
        
        buttonView.layer.cornerRadius = 8
        
        usernameError.hidden = true
        passwordError.hidden = true
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func shouldPerformSegueWithIdentifier(identifier: String, sender: AnyObject?) -> Bool {
        
        if usernameTextInput.text == "Mickey" && passwordTextInput.text == "Mouse" {
            
            usernameError.hidden = true
            usernameTextInput.layer.borderColor = UIColor.clearColor().CGColor
            usernameTextInput.layer.cornerRadius = 8
            usernameTextInput.layer.borderWidth = 0
            usernameTextInput.text = ""
            passwordError.hidden = true
            passwordTextInput.layer.borderColor = UIColor.clearColor().CGColor
            passwordTextInput.layer.cornerRadius = 8
            passwordTextInput.layer.borderWidth = 0
            passwordTextInput.text = ""

            
            return true
            
        } else {
        
            if usernameTextInput.text != "Mickey" {
            usernameError.hidden = false
            usernameTextInput.layer.borderColor = UIColor.redColor().CGColor
            usernameTextInput.layer.cornerRadius = 8
            usernameTextInput.layer.borderWidth = 2
            usernameTextInput.text = ""
            } else {
            usernameError.hidden = true
            usernameTextInput.layer.borderColor = UIColor.clearColor().CGColor
            usernameTextInput.layer.cornerRadius = 8
            usernameTextInput.layer.borderWidth = 0
            usernameTextInput.text = "Mickey"
            }
            
            if passwordTextInput.text != "Mouse" {
            passwordError.hidden = false
            passwordTextInput.layer.borderColor = UIColor.redColor().CGColor
            passwordTextInput.layer.cornerRadius = 8
            passwordTextInput.layer.borderWidth = 2
            passwordTextInput.text = ""
            } else {
            passwordError.hidden = true
            passwordTextInput.layer.borderColor = UIColor.clearColor().CGColor
            passwordTextInput.layer.cornerRadius = 8
            passwordTextInput.layer.borderWidth = 0
            passwordTextInput.text = "Mouse"
            }
            return false
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

