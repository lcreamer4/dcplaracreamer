//
//  BusDetailViewController.swift
//  DCPLaraCreamer
//
//  Created by Lara Creamer on 5/11/16.
//  Copyright © 2016 Lara Creamer. All rights reserved.
//

import UIKit
import Firebase

class BusDetailViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {
    
    
    
    @IBOutlet weak var busRouteLabel: UILabel!
    
    @IBOutlet weak var currentStatusLabel: UILabel!
    
    @IBOutlet weak var TimeUpdate: UIPickerView!
    
    @IBOutlet weak var busNameLabel: UILabel!
    
    @IBOutlet weak var timesLabel: UILabel!
    
    
    
    var pickerViewData = ["On Time", "<2 mins", "2-5 mins", "5-10 mins", ">10 mins"]
    
    var currentStatus = ""
    
    var busName = ""
    
    var arrivalTimes = ""
    
    var busRoute = ""
    
    @IBAction func goBackButton(sender: UIBarButtonItem) {

    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        busNameLabel.text = busName
        timesLabel.text = arrivalTimes
        busRouteLabel.text = busRoute
        currentStatusLabel.text = currentStatus
        

        // Do any additional setup after loading the view.
        self.TimeUpdate.dataSource = self
        self.TimeUpdate.delegate = self
    }
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerViewData.count
    }

    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerViewData[row]
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerViewData[row] == "On Time" {
        currentStatusLabel.text = pickerViewData[row]
            
            let myStatusRef = Firebase(url:"https://burning-inferno-6410.firebaseio.com/buses/\(busName)/status")
            myStatusRef.setValue("On Time")
            let myStatusImageRef = Firebase(url:"https://burning-inferno-6410.firebaseio.com/buses/\(busName)/status Image")
            myStatusImageRef.setValue("clockOnTime")
            let myTimeRef = Firebase(url:"https://burning-inferno-6410.firebaseio.com/buses/\(busName)/time late")
            myTimeRef.setValue("\(pickerViewData[row])")
        } else {
        
        currentStatusLabel.text = "\(pickerViewData[row]) late"
            
        let myStatusRef = Firebase(url:"https://burning-inferno-6410.firebaseio.com/buses/\(busName)/status")
        myStatusRef.setValue("Late")
        let myStatusImageRef = Firebase(url:"https://burning-inferno-6410.firebaseio.com/buses/\(busName)/status Image")
        myStatusImageRef.setValue("clockLate")
        let myTimeRef = Firebase(url:"https://burning-inferno-6410.firebaseio.com/buses/\(busName)/time late")
        myTimeRef.setValue("\(pickerViewData[row])")
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
