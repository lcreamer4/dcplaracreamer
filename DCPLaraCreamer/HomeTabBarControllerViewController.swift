//
//  HomeTabBarControllerViewController.swift
//  DCPLaraCreamer
//
//  Created by Lara Creamer on 5/9/16.
//  Copyright © 2016 Lara Creamer. All rights reserved.
//

import UIKit

class HomeTabBarControllerViewController: UITabBarController {
    
    var tabBarImage = UIImage(named: "tabBarImage")
    var navBarImage = UIImage(named: "NavBarImage")

    override func viewDidLoad() {
        super.viewDidLoad()

        self.tabBar.backgroundImage = tabBarImage
        self.tabBar.tintColor = UIColor.whiteColor()
        UINavigationBar.appearance().setBackgroundImage(navBarImage, forBarMetrics: .Default)
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
