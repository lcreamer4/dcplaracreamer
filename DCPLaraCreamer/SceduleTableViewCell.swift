//
//  SceduleTableViewCell.swift
//  DCPLaraCreamer
//
//  Created by Lara Creamer on 5/9/16.
//  Copyright © 2016 Lara Creamer. All rights reserved.
//

import UIKit

class SceduleTableViewCell: UITableViewCell {
    
    @IBOutlet weak var date: UILabel!
    
    @IBOutlet weak var time: UILabel!

    @IBOutlet weak var location: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
