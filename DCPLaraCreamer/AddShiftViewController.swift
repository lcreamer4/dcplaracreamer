//
//  AddShiftViewController.swift
//  DCPLaraCreamer
//
//  Created by Lara Creamer on 5/17/16.
//  Copyright © 2016 Lara Creamer. All rights reserved.
//

import UIKit
import Firebase

class AddShiftViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    var dateArray: [String] = []
    var timeArray: [String] = []
    var locationArray: [String] = []
    var nameArray: [String] = []
    var shiftArray: [String] = []
    var occupationArray: [String] = []
    var newName = ""
    var dateCatch = ""
    var timeCatch = ""
    var nameCatch = ""
    var locationCatch = ""
    var occupationCatch = ""
    var newShift = [ : ]
    
    var mySchedRef = Firebase(url:"https://burning-inferno-6410.firebaseio.com/mySchedule")
    
    @IBOutlet weak var shiftPicker: UIPickerView!
    
    @IBOutlet weak var buttonView: UIView!
    
     var pickerViewData = [""]
    
    @IBAction func dropShiftButon(sender: UIButton) {
    let mySchedDeleteRef = Firebase(url:"https://burning-inferno-6410.firebaseio.com/mySchedule/\(nameCatch)")
        mySchedDeleteRef.removeValue()
        
        let shiftRef = Firebase(url:"https://burning-inferno-6410.firebaseio.com/shifts")
        let shiftAddedRef = shiftRef.childByAutoId()
        
        
        shiftAddedRef.updateChildValues(newShift as [NSObject : AnyObject])
        
        
        
        let alert = UIAlertController(title: "Shift Added", message: "Your shift has been added to available shifts.", preferredStyle: .Alert)
        
        alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (alertAction) -> Void in
            self.navigationController?.popViewControllerAnimated(true)
        }))
        
        presentViewController(alert, animated: true, completion: nil)
    }
    
    override func viewWillAppear(animated: Bool) {
        
        buttonView.layer.cornerRadius = 8
        
        let shiftsCountRef = Firebase(url:"https://burning-inferno-6410.firebaseio.com/shifts")
        
        shiftsCountRef.queryOrderedByKey().observeEventType(.ChildAdded, withBlock: { snapshot in
            
            if let shiftName = snapshot.value["shift name"] as? String {
                self.shiftArray.append(shiftName)
                self.newName = "shift\(self.shiftArray.count + 1)"
                
            }
        })
        
        mySchedRef.queryOrderedByKey().observeEventType(.ChildAdded, withBlock: { snapshot in
            
            if let scheduleDate = snapshot.value["shift date"] as? String {
                self.dateArray.append(scheduleDate)
            }
            
            if let scheduleTime = snapshot.value["shift time"] as? String {
                self.timeArray.append(scheduleTime)
            }
            if let scheduleLocation = snapshot.value["shift location"] as? String {
                self.locationArray.append(scheduleLocation)
            }
            if let scheduleName = snapshot.value["shift name"] as? String {
                self.nameArray.append(scheduleName)
            }
            if let scheduleOccupation = snapshot.value["shift occcupation"] as? String {
                self.occupationArray.append(scheduleOccupation)
            }
            dispatch_async(dispatch_get_main_queue(),{ 
                self.shiftPicker.reloadAllComponents()
            })
            
        })
        self.shiftPicker.dataSource = self
        self.shiftPicker.delegate = self
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return dateArray.count
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        nameCatch = nameArray[row]
        dateCatch = dateArray[row]
        timeCatch = timeArray[row]
        locationCatch = locationArray[row]
        occupationCatch = occupationArray[row]
        
        newShift = ["shift name":"\(newName)","shift occcupation": occupationCatch, "shift date": dateCatch, "shift time": timeCatch, "shift location": locationCatch]
        
        return "\(dateArray[row]) \(timeArray[row])"
        

    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        nameCatch = nameArray[row]
    }

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
