//
//  Shift DetailViewController.swift
//  DCPLaraCreamer
//
//  Created by Lara Creamer on 5/17/16.
//  Copyright © 2016 Lara Creamer. All rights reserved.
//

import UIKit
import Firebase

class Shift_DetailViewController: UIViewController {
    
    @IBOutlet weak var occupationLabel: UILabel!
    
    @IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var timeLabel: UILabel!
    
    @IBOutlet weak var locationLabel: UILabel!
    
    @IBOutlet weak var buttonView: UIView!
    
    var currentOccupation = ""
    var currentDate = ""
    var currentTime = ""
    var currentLocation = ""
    var currentName = ""
    var newName = ""
    var schedArray: [String] = []
    var newDay = [ : ]
    
    
    
    @IBAction func takeShiftButton(sender: UIButton) {
        
        let shiftRef = Firebase(url:"https://burning-inferno-6410.firebaseio.com/shifts/\(currentName)")
        shiftRef.removeValue()
        
        let personalSchedRef = Firebase(url:"https://burning-inferno-6410.firebaseio.com/mySchedule/\(newName)")
         personalSchedRef.updateChildValues(newDay as [NSObject : AnyObject])
       
        let alert = UIAlertController(title: "Shift Added", message: "This shift has been added to your personal shcedule.", preferredStyle: .Alert)
        
        alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (alertAction) -> Void in
            self.navigationController?.popViewControllerAnimated(true)
        }))
        
        presentViewController(alert, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        buttonView.layer.cornerRadius = 8
        
        let personalSchedCountRef = Firebase(url:"https://burning-inferno-6410.firebaseio.com/mySchedule")
        
        personalSchedCountRef.queryOrderedByKey().observeEventType(.ChildAdded, withBlock: { snapshot in
            
            if let schedName = snapshot.value["shift name"] as? String {
                self.schedArray.append(schedName)
                self.newName = "day\(self.schedArray.count + 1)"
            }
        })
        occupationLabel.text = currentOccupation
        dateLabel.text = currentDate
        timeLabel.text = currentTime
        locationLabel.text = currentLocation
        
        newDay = ["shift name":"day\(newName)","shift occcupation": currentOccupation, "shift date": currentDate, "shift time": currentTime, "shift location": currentLocation]
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
