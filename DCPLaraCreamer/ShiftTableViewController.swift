//
//  ShiftTableViewController.swift
//  DCPLaraCreamer
//
//  Created by Lara Creamer on 5/17/16.
//  Copyright © 2016 Lara Creamer. All rights reserved.
//

import UIKit
import Firebase

class ShiftTableViewController: UITableViewController {
    
    var occupationArray: [String] = []
    var dateArray: [String] = []
    var locationArray: [String] = []
    var timeArray: [String] = []
    var nameArray: [String] = []
    var currentOccupation = ""
    var currentDate = ""
    var currentTime = ""
    var currentLocation = ""
    var currentName = ""
    var rowNumber = 0
    
    @IBAction func logOutButton(sender: UIBarButtonItem) {
        let alert = UIAlertController(title: "Logging Out?", message: "Are you sure you want to log out?", preferredStyle: .Alert)
        
        alert.addAction(UIAlertAction(title: "Log Out", style: .Default, handler: { (alertAction) -> Void in
            
            self.performSegueWithIdentifier("toLogOutScreen", sender: self)
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .Cancel, handler: nil))
        
        presentViewController(alert, animated: true, completion: nil)
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let myRef = Firebase(url:"https://burning-inferno-6410.firebaseio.com/shifts")
        
        myRef.queryOrderedByKey().observeEventType(.Value, withBlock: { snapshot in
            
            self.occupationArray.removeAll()
            self.dateArray.removeAll()
            self.locationArray.removeAll()
            self.timeArray.removeAll()
            self.nameArray.removeAll()
            
            guard let keys = (snapshot.value as? NSDictionary)?.allKeys as? [String] else {
                return
            }
            
            for key in keys {
                
                
                if let shiftOccupation = snapshot.value[key]??["shift occcupation"] as? String {
                    self.occupationArray.append(shiftOccupation)
                }
                if let shiftDate = snapshot.value[key]??["shift date"] as? String {
                    self.dateArray.append(shiftDate)
                }
                if let shiftLocation = snapshot.value[key]??["shift location"] as? String {
                    self.locationArray.append(shiftLocation)
                }
                if let shiftTime = snapshot.value[key]??["shift time"] as? String {
                    self.timeArray.append(shiftTime)
                }
                if let shiftName = snapshot.value[key]??["shift name"] as? String {
                    self.nameArray.append(shiftName)
                }
            }
            dispatch_async(dispatch_get_main_queue(), {
                self.tableView.reloadData()
            })
            
        })
        


        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return dateArray.count
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as! ShiftTableViewCell

        // Configure the cell...
        cell.occupationLabel.text = occupationArray[indexPath.row]
        cell.dateLabel.text = dateArray[indexPath.row]
        cell.locationLabel.text = locationArray[indexPath.row]

        return cell
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "toDetailView" {
            let destinationVC = segue.destinationViewController as! Shift_DetailViewController
            destinationVC.currentOccupation = currentOccupation
            destinationVC.currentDate = currentDate
            destinationVC.currentTime = currentTime
            destinationVC.currentLocation = currentLocation
            destinationVC.currentName = currentName
        }

    }
    
    override func tableView(tableView: UITableView, didHighlightRowAtIndexPath indexPath: NSIndexPath) {
        currentOccupation = occupationArray[indexPath.row]
        currentDate = dateArray[indexPath.row]
        currentTime = timeArray[indexPath.row]
        currentLocation = locationArray[indexPath.row]
       currentName = nameArray[indexPath.row]
        
        
       
    }
    
    @IBAction func unwindSegue(segue: UIStoryboardSegue){

    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
